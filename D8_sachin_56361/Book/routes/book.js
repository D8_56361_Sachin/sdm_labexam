const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.post('/', (request, response) => {
  const {book_id,book_title,publisher_name, author_name } = request.body

  const query = `
    INSERT INTO Product
      ( book_id,book_title,publisher_name, author_name)
    VALUES
      (${book_id} ${book_title}, '${publisher_name}', ${author_name})
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.get('/', (request, response) => {
  const query = `
     book_id,book_title,publisher_name, author_name
    FROM book
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.put('/:book_id', (request, response) => {
  const { book_id } = request.params
  const { book_title,publisher_name, author_name} = request.body

  const query = `
    UPDATE book
    SET
       
    
    book_title= ${book_title},
    publisher_name= ${publisher_name},
    author_name = ${author_name}
    WHERE
    book_id= ${id}
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.delete('/:book_id', (request, response) => {
  const { id } = request.params

  const query = `
    DELETE FROM book
    WHERE
      book_Id = ${id}
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

module.exports = router
