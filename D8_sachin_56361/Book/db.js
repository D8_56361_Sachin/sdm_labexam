const mysql = require('mysql2')

const openConnection = () =>{
   const connection = mysql.createConnection({

        host: 'DemoDB',
        port: 3306,
        user: 'root',
        password: 'root',
        database: 'jan14'
    })
    
    connection.connect();
    return connection
}

module.exports = {
    openConnection
}