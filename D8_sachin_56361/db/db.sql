create table emp (
  id integer primary key auto_increment,
  name VARCHAR(200),
  salary integer,
  age int(5)
);

INSERT INTO emp VALUES(1, "Sachin", 10000, 25);
INSERT INTO emp VALUES(2, "Ramesh", 10000, 25);
INSERT INTO emp VALUES(3, "Mahesh", 10000, 24);

